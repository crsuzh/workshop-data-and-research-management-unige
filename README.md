# Workshop Data and research management

This repository includes all material to reproduce the first results and Figure in Gneezy et al., and the test that was replicated as part of the Social Sciences Replication Project.
# Describtion of the folders
1. Anlysis consists of two subfolder:

orig_analysis: small change in RMD ---> contains report_orig_results.rmd

rep_analysis: intial commit ---> contains report_rep_results.rmd

2. data
orig_code_book.json  - Upload New File 

Code book for original study data
    Meaning of the different variables

orig_data.txt - Update orig_data.txt